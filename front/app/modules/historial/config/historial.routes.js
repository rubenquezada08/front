(function() {
    'use strict';
    //Setting up route
    angular.module('historial').config([
        '$stateProvider',
        function($stateProvider) {
            // Soportetecnico state routing
            $stateProvider
            .state('app.historial', {
                ncyBreadcrumb: {
                    label: 'historial'
                },
                url: '/historial',
                views: {
                    'content@app': {
                        controller: 'Historial',
                        controllerAs: 'vmHistorial',
                        templateUrl: '/app/modules/historial/views/historial.view.html'
                    }
                }
            });
        }
    ]);
})();
