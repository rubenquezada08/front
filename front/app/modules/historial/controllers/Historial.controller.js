(function() {
    'use strict';
angular.module('historial').controller('Historial', ['blockUI','growl','SweetAlert','$rootScope','$state','Serviciohistorial',

        function(
            blockUI,            
            growl,
            SweetAlert,
            $rootScope,
            $state,
            Serviciohistorial
        ) {
            var vmHistorial = this;
            

           

            console.log($rootScope.id_usuario);
            vmHistorial.id_usuario = $rootScope.id_usuario;
            vmHistorial.username = $rootScope.username;       

            vmHistorial.gridOptions={
            enablePaginationControls :false,
            enableRowSelection: true,
            enableRowHeaderSelection: false ,
            enableColumnMenus:false,
            columnDefs: [
            { name: 'folio', field: 'folio' , width:'*', enableHiding : false},
            { name: 'estatus', field: 'estatus' , width:'*', enableHiding : false },
            { name: 'monto', field: 'monto_prestamo' , width:'*', enableHiding : false},
            { name: 'interes', field: 'interes' , width:'*', enableHiding : false},
            { name: 'deuda', field: 'deuda' , width:'*', enableHiding : false},
            { name: 'fecha', field: 'fecha' , width:'*', enableHiding : false},

            ],
                data:[]
            };

           
            vmHistorial.inicio =  function(){
                 if(vmHistorial.id_usuario>0)
                 {
                    Serviciohistorial.consultahistorial(vmHistorial.id_usuario).then(function(respuesta){
                        if(respuesta){
                            console.log(respuesta);
                            vmHistorial.gridOptions.data = respuesta
                        }                                                
                    },function(error){
                        
                    });
                 }
                 else{
                     $state.go('app.home');
                 }                                  
             }

            vmHistorial.cerrar =  function(){
                $rootScope.username = undefined;
                $rootScope.id_usuario = undefined;
                vmHistorial.inicio();
            }
             vmHistorial.inicio();

           



         
        }
    ]);
})();
