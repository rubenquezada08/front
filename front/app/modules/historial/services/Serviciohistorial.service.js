(function() {
    'use strict';
    angular.module('historial').factory('Serviciohistorial', [
        '$http',
        '$q',
        'Config',
        function(
            $http, 
            $q, 
            Config
        ) {
            return {
                consultahistorial: function(id_usuario){	
                     
                    return $http({
                        method: 'GET',
                        url:Config.api+'/consultahistorial/'+id_usuario,
                    }).then(function(response){
                        return response.data.data.response;
                        //return response;
                    },function(response){
                        return $q.reject(response);
                        });
                }
            }
        }
    ]);
})();
