(function() {
    'use strict';
    angular.module('home').controller('Home', ['Serviciohome','$rootScope',
        function(Serviciohome,$rootScope) {
            var vmHome = this;

            
            vmHome.titulo = 'Titulo';
            // Logica de controlador
            // ...

            
             vmHome.login =  function(){
                 console.log($rootScope.id_usuario);
                  $rootScope.username = vmHome.username;
                  Serviciohome.consultaPerfil(vmHome.username).then(function(respuesta){
                        if(respuesta){
                            console.log(respuesta);
                            $rootScope.id_usuario = respuesta.id_usuario;
                            console.log($rootScope.id_usuario);
                            vmHome.inicio();
                        }                        
                        // blockUI.stop();
                    },function(error){
                        //blockUI.stop(); 
                    });
             }
            
            vmHome.inicio =  function(){
                console.log($rootScope.id_usuario);
                if($rootScope.id_usuario > 0 ) {
                    vmHome.visible = false;
                }
                else{
                    vmHome.visible = true;
                }
            }
               
            vmHome.cerrar =  function(){
                vmHome.username = undefined;
                $rootScope.username = undefined;
                $rootScope.id_usuario = undefined;
                vmHome.inicio();
            }
            vmHome.inicio();
        }
    ]);
})();
