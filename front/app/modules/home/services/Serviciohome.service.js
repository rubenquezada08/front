(function() {
    'use strict';
    angular.module('home').factory('Serviciohome', [
        '$http',
        '$q',
        'Config',
        function(
            $http, 
            $q, 
            Config
        ) {
            return {
                consultaPerfil: function(username){	
                    return $http({
                        method: 'GET',
                        url:Config.api+'/grabaperfil/'+username,
                    }).then(function(response){
                        return response.data.data.response;
                        //return response;
                    },function(response){
                        return $q.reject(response);
                        });
                }
            }
        }
    ]);
})();
