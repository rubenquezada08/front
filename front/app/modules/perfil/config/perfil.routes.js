(function() {
    'use strict';
    //Setting up route
    angular.module('perfil').config([
        '$stateProvider',
        function($stateProvider) {
            // Soportetecnico state routing
            $stateProvider
            .state('app.perfil', {
                ncyBreadcrumb: {
                    label: 'perfil'
                },
                url: '/perfil',
                views: {
                    'content@app': {
                        controller: 'Perfil',
                        controllerAs: 'vmPerfil',
                        templateUrl: '/app/modules/perfil/views/perfil.view.html'
                    }
                }
            });
        }
    ]);
})();
