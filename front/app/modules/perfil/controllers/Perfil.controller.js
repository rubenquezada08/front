(function() {
    'use strict';
angular.module('perfil').controller('Perfil', ['blockUI','growl','SweetAlert','$rootScope','Servicioperfil','$state',

        function(
            blockUI,            
            growl,
            SweetAlert,
            $rootScope,
            Servicioperfil,
            $state
        ) {
            var vmPerfil = this;
            
            console.log($rootScope.id_usuario);
            vmPerfil.id_usuario = $rootScope.id_usuario;
            vmPerfil.username = $rootScope.username;

            //vmPerfil.id_usuario=1;

            vmPerfil.gridOptions={
            enablePaginationControls :false,
            enableRowSelection: true,
            enableRowHeaderSelection: false ,
            enableColumnMenus:false,
            columnDefs: [
            { name: 'folio', field: 'folio' , width:'*', enableHiding : false},
            { name: 'estatus', field: 'estatus' , width:'*', enableHiding : false },
            { name: 'monto', field: 'monto_prestamo' , width:'*', enableHiding : false},
            { name: 'interes', field: 'interes' , width:'*', enableHiding : false},
            { name: 'deuda', field: 'deuda' , width:'*', enableHiding : false},
            { name: 'fecha', field: 'fecha' , width:'*', enableHiding : false},

            ],
                data:[]
            };

           
            vmPerfil.inicio =  function(){
                console.log(vmPerfil.id_usuario);
                 if(vmPerfil.id_usuario>0)
                 {
                    Servicioperfil.consultahistorialpendientes(vmPerfil.id_usuario).then(function(respuesta){
                        if(respuesta){
                            console.log(respuesta);
                            vmPerfil.gridOptions.data = respuesta
                        }                                                
                    },function(error){
                        
                    });
                 }
                 else{
                     $state.go('app.home');
                 }                                  
             }

             vmPerfil.cerrar =  function(){
                $rootScope.username = undefined;
                $rootScope.id_usuario = undefined;
                vmPerfil.inicio();
            }
             vmPerfil.inicio();

         
        }
    ]);
})();
