(function() {
    'use strict';
    angular.module('perfil').factory('Servicioperfil', [
        '$http',
        '$q',
        'Config',
        function(
            $http, 
            $q, 
            Config
        ) {
            return {                
                 consultahistorialpendientes: function(id_usuario){	
                     
                    return $http({
                        method: 'GET',
                        url:Config.api+'/consultahistorialpendientes/'+id_usuario,
                    }).then(function(response){
                        return response.data.data.response;
                        //return response;
                    },function(response){
                        return $q.reject(response);
                        });
                }
            }
        }
    ]);
})();
