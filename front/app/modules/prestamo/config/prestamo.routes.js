(function() {
    'use strict';
    //Setting up route
    angular.module('prestamo').config([
        '$stateProvider',
        function($stateProvider) {
            // Soportetecnico state routing
            $stateProvider
            .state('app.prestamo', {
                ncyBreadcrumb: {
                    label: 'prestamo'
                },
                url: '/prestamo',
                views: {
                    'content@app': {
                        controller: 'Prestamo',
                        controllerAs: 'vmPrestamo',
                        templateUrl: '/app/modules/prestamo/views/prestamo.view.html'
                    }
                }
            });
        }
    ]);
})();
