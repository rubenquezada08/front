(function() {
    'use strict';
angular.module('prestamo').controller('Prestamo', ['blockUI','growl','SweetAlert','Servicioperfil','$rootScope','$state','Servicioprestamo',

        function(
            blockUI,            
            growl,
            SweetAlert,
            Servicioperfil,
            $rootScope,
            $state,
            Servicioprestamo
        ) {
            var vmPrestamo = this;
            
            console.log($rootScope.id_usuario);
            vmPrestamo.id_usuario = $rootScope.id_usuario;
            vmPrestamo.username = $rootScope.username;
            vmPrestamo.deuda = 0;
            //vmPrestamo.id_usuario=1;

            vmPrestamo.gridOptions={
            enablePaginationControls :false,
            enableRowSelection: true,
            enableRowHeaderSelection: false ,
            enableColumnMenus:false,
            columnDefs: [
            { name: 'folio', field: 'folio' , width:'*', enableHiding : false},
            { name: 'estatus', field: 'estatus' , width:'*', enableHiding : false },
            { name: 'monto', field: 'monto_prestamo' , width:'*', enableHiding : false},
            { name: 'interes', field: 'interes' , width:'*', enableHiding : false},
            { name: 'deuda', field: 'deuda' , width:'*', enableHiding : false},
            { name: 'fecha', field: 'fecha' , width:'*', enableHiding : false},

            ],
                data:[]
            };

           
            vmPrestamo.inicio =  function(){
                 if(vmPrestamo.id_usuario>0)
                 {
                    Servicioperfil.consultahistorialpendientes(vmPrestamo.id_usuario).then(function(respuesta){
                        if(respuesta){
                            console.log(respuesta);
                            vmPrestamo.gridOptions.data = respuesta
                        }                                                
                    },function(error){
                        
                    });
                 }
                 else{
                     $state.go('app.home');
                 }                                  
             }
            vmPrestamo.cerrar =  function(){
                $rootScope.username = undefined;
                $rootScope.id_usuario = undefined;
                vmPrestamo.inicio();
            }
            
           

           vmPrestamo.cambioSelect =  function(){
                
                if(vmPrestamo.monto>0 && vmPrestamo.select)
                {
                    vmPrestamo.interes=(vmPrestamo.select*vmPrestamo.monto)/100;
                    vmPrestamo.deuda = vmPrestamo.monto+vmPrestamo.interes;
                }
                else{
                    vmPrestamo.deuda = 0;
                }
                
           }

           vmPrestamo.prestamo = function(){
               console.log(vmPrestamo.tarjeta);
               if(vmPrestamo.monto>0 && vmPrestamo.select && vmPrestamo.monto>0 && vmPrestamo.monto && vmPrestamo.edad>0 && vmPrestamo.edad)
               {
                   if(vmPrestamo.tarjeta){
                       vmPrestamo.tarjetaNumero= 1;
                   }
                   else{
                       vmPrestamo.tarjetaNumero = 0;
                   }
                   Servicioprestamo.grabaprestamo(vmPrestamo.id_usuario,vmPrestamo.monto,vmPrestamo.interes,vmPrestamo.deuda,
                        vmPrestamo.edad,vmPrestamo.tarjetaNumero).then(function(respuesta){
                        vmPrestamo.limpiar();
                        if(respuesta){
                            console.log(respuesta);
                            if(respuesta.estado == 1)
                            {
                                vmPrestamo.inicio();
                            }
                            
                        }                                                
                    },function(error){
                        
                    });
               }
               else{
                   SweetAlert.swal("Advertencia!","Es necesario llenar el formulario","warning");
               }
           }

           vmPrestamo.limpiar= function(){     
               vmPrestamo.select =  undefined;       
               vmPrestamo.monto =  undefined;
               vmPrestamo.interes = undefined;
               vmPrestamo.deuda = 0;
               vmPrestamo.edad = undefined;
               vmPrestamo.tarjeta = false;
           }

          vmPrestamo.inicio();
        }
    ]);
    
angular.module('prestamo').directive('entero', function() {
            return {
                require: 'ngModel',
                restrict: 'A',
                link: function(scope, element, attr, ctrl) {
                    function inputValue(val) {
                        if (val) {
                            var value = val + ''; //convert to string
                            var digits = value.replace(/[^0-9]/g, '');

                            if (digits !== value) {
                                ctrl.$setViewValue(digits);
                                ctrl.$render();
                            }
                            return parseInt(digits);
                        }
                        return "";
                    }
                    ctrl.$parsers.push(inputValue);
                }
            };
        });
})();
