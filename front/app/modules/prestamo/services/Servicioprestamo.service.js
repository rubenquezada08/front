(function() {
    'use strict';
    angular.module('prestamo').factory('Servicioprestamo', [
        '$http',
        '$q',
        'Config',
        function(
            $http, 
            $q, 
            Config
        ) {
             return {
                grabaprestamo: function(id_usuario,monto,interes,deuda,edad,tarjetaNumero){	
                    return $http({
                        method: 'GET',
                        url:Config.api+'/grabaprestamo/'+id_usuario+'/'+monto+'/'+interes+'/'+deuda+'/'+edad+'/'+tarjetaNumero,
                    }).then(function(response){
                        return response.data.data.response;
                        //return response;
                    },function(response){
                        return $q.reject(response);
                        });
                }
            }
        }
    ]);
})();
